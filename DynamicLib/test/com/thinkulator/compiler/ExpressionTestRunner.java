/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package com.thinkulator.compiler;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import static org.junit.Assert.*;

/**
 *
 * @author hack
 */
public class ExpressionTestRunner extends Runner {
   
    private ExpressionTest [] _tests;
    private Description _desc;
    private Map<ExpressionTest,Description> _testsDesc;
    
    Object o;
    public ExpressionTestRunner(Class c) throws InstantiationException, IllegalAccessException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException{
        o = c.newInstance();
        _tests = (ExpressionTest []) c.getMethod("getExpressionTests").invoke(o);
        _testsDesc = new HashMap<ExpressionTest,Description>();
        
        _desc = Description.createSuiteDescription(c);
        for(ExpressionTest t:_tests){
            _testsDesc.put(t,Description.createTestDescription(c,t.getDesc()+" ["+t.getExpression()+"]"));
            _desc.addChild(_testsDesc.get(t));
        }
    }

    @Override
    public Description getDescription() {
        return _desc;
    }

    @Override
    public void run(RunNotifier notifier) {
        for(ExpressionTest test:_tests){
            notifier.fireTestStarted(_testsDesc.get(test));
            
            try{
                //execute the expression, verify the expected result
                String result = (String) o.getClass().getMethod("executeFunction",String.class, Map.class).invoke(o, test.getExpression(), test.getData());
                //String result =  ExpressionJavascriptCompilerTest.executeFunction();
                assertEquals(test.getExpected(), result);
            }catch(Throwable t){
                if(t instanceof InvocationTargetException){
                    t = t.getCause();
                }
                notifier.fireTestFailure(new Failure(_testsDesc.get(test),t));
            }
            
            notifier.fireTestFinished(_testsDesc.get(test));
        }
        
        
    }
    
    protected static class ExpressionTest{
        private String _desc;
        private String _expression;
        private String _expected;
        private Map<String,Object> _data;

        
        public ExpressionTest(String desc, String expression, String expected) {
            this._desc = desc;
            this._expression = expression;
            this._expected = expected;
        }
        
        public ExpressionTest(String desc, String expression, String expected, Map<String, Object> data) {
            this._desc = desc;
            this._expression = expression;
            this._expected = expected;
            this._data = data;
        }

        public Map<String, Object> getData() {
            return _data;
        }

        public String getDesc() {
            return _desc;
        }

        public String getExpected() {
            return _expected;
        }

        public String getExpression() {
            return _expression;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 29 * hash + (this._desc != null ? this._desc.hashCode():0);
            hash = 29 * hash + (this._expected != null ? this._expected.hashCode():0);
            hash = 29 * hash + (this._expression != null ? this._expression.hashCode():0);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj == null){
                return false;
            }
            if(!getClass().equals(obj.getClass())){
                return false;
            }
            ExpressionTest other = (ExpressionTest)obj;
            if((this._desc == null) ? (other._desc != null) : !this._desc.equals(other._desc)){
                return false;
            }
            if((this._expected == null) ? (other._expected != null) : !this._expected.equals(other._expected)){
                return false;
            } 
            if((this._expression == null) ? (other._expression != null) : !this._expression.equals(other._expression)){
                return false;
            }
            return true;
        }
        
        
    
    }
}
