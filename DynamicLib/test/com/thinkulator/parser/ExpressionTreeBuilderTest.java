/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package com.thinkulator.parser;

import com.thinkulator.parser.ExpressionTokenizer;
import com.thinkulator.parser.ParseTreeEntry;
import com.thinkulator.parser.ExpressionTreeBuilder;
import com.thinkulator.parser.ExpressionTreeBuilderException;
import java.text.ParseException;
import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hack
 */
public class ExpressionTreeBuilderTest {

    public ExpressionTreeBuilderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void basicInfixExpression() throws ExpressionTreeBuilderException, ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("3*4+2");
        ExpressionTreeBuilder tb = ExpressionTreeBuilder.getBuilder(et);
        ParseTreeEntry pte = tb.parse();

        assertTreeEntry(pte, "", "+", "+", 2);
        assertTreeEntry(pte, "0", "*", "*", 2);
        assertTreeEntry(pte, "0,0", "(literal)", "3", 0);
        assertTreeEntry(pte, "0,1", "(literal)", "4", 0);
        assertTreeEntry(pte, "1", "(literal)", "2", 0);
    }

    @Test
    public void basicItselfExpression() throws ExpressionTreeBuilderException, ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("3*X+2");
        ExpressionTreeBuilder tb = ExpressionTreeBuilder.getBuilder(et);
        ParseTreeEntry pte = tb.parse();
        printTree(pte, 0);
        assertTreeEntry(pte, "", "+", "+", 2);
        assertTreeEntry(pte, "0", "*", "*", 2);
        assertTreeEntry(pte, "0,0", "(literal)", "3", 0);
        assertTreeEntry(pte, "0,1", "(name)", "X", 0);
        assertTreeEntry(pte, "1", "(literal)", "2", 0);
    }

    @Test
    public void basicFunctionCallExpression() throws ExpressionTreeBuilderException, ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("TODAY()");
        ExpressionTreeBuilder tb = ExpressionTreeBuilder.getBuilder(et);
        ParseTreeEntry pte = tb.parse();

        assertTreeEntry(pte, "", "TODAY", "TODAY", 0);
    }

    @Test
    public void parameterizedFunctionCallExpression() throws ExpressionTreeBuilderException, ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("IF(eval,true,false)");
        ExpressionTreeBuilder tb = ExpressionTreeBuilder.getBuilder(et);
        ParseTreeEntry pte = tb.parse();
        
        assertTreeEntry(pte, "", "IF", "IF", 3);
        assertTreeEntry(pte, "0", "(name)", "eval", 0);
        assertTreeEntry(pte, "1", "(name)", "true", 0);
        assertTreeEntry(pte, "2", "(name)", "false", 0);
    }
    
    private void printTree(ParseTreeEntry pte,int level){
        for(int i=0;i<level;i++){
            System.out.print(" ");
        }
        System.out.println("ID:   \t"+pte.getSymbol().getID());
        if(pte.getToken() != null){
            for(int i=0;i<level;i++){
                System.out.print(" ");
            }
            System.out.println("Value:\t"+pte.getToken().getValue());
        }
        ArrayList<ParseTreeEntry> br = pte.getBranches();
        for(int a=0;a<br.size();a++){

            for(int i=0;i<level;i++){
                System.out.print(" ");
            }
            System.out.println(a+":");
            printTree(br.get(a),level+1);
        }
    }

    /** Path is defined by a series of numbers, 0,1,1,2
     * 
     * @param pte
     * @param path
     * @param symbolID
     * @param tokenValue
     * @param numBranches
     */
    private void assertTreeEntry(ParseTreeEntry pte,String path,String symbolID,String tokenValue,int numBranches){
        //navigate to the entry specified by the path
        String [] parts = path.split(",");
        String at = "";
        for(String part:parts){
            if(part.length() == 0){
                continue;
            }
            if(at.length() >= 0){
                at=at+",";
            }
            at=at+part;
            Integer i = Integer.parseInt(part);
            assertTrue("Entry Exists at path point "+at,pte.getBranches().size() > i);
            pte = pte.getBranches().get(i);
        }
        if(at.length() == 0){
            at = "Root";
        }
        assertEquals("Symbol at "+path,symbolID, pte.getSymbol().getID());
        assertNotNull("Node at "+path,pte.getToken());
        assertEquals("Token value at"+path,tokenValue, pte.getToken().getValue());
    }
}