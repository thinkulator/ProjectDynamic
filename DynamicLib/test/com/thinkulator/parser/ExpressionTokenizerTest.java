/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package com.thinkulator.parser;

import com.thinkulator.parser.ExpressionTokenizer;
import com.thinkulator.parser.Token;
import java.text.ParseException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hack
 */
public class ExpressionTokenizerTest {

    public ExpressionTokenizerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void simpleSingleDigitNumber() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("4");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Number, t.getType());
        assertEquals("4",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(1,t.getTo());
        assertFalse(et.hasNext());
    }

    @Test
    public void simpleMultiDigitNumber() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("442");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Number, t.getType());
        assertEquals("442",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(3,t.getTo());
        assertFalse(et.hasNext());
    }

    @Test
    public void simpleDecimalDigitNumber() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("3.141");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Number, t.getType());
        assertEquals("3.141",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(5,t.getTo());
        assertFalse(et.hasNext());
    }

    @Test
    public void simpleExponentNumber() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("6.022e23");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Number, t.getType());
        assertEquals("6.022e23",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(8,t.getTo());
        assertFalse(et.hasNext());
    }

    @Test
    public void simpleNegativeExponentNumber() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("6.022e-23");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Number, t.getType());
        assertEquals("6.022e-23",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(9,t.getTo());
        assertFalse(et.hasNext());
    }

    @Test
    public void simplePositiveExponentNumber() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("6.022e+23");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Number, t.getType());
        assertEquals("6.022e+23",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(9,t.getTo());
        assertFalse(et.hasNext());
    }

    @Test
    public void simpleName() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("Foo");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Name, t.getType());
        assertEquals("Foo",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(3,t.getTo());
        assertFalse(et.hasNext());
    }

    @Test
    public void simpleSingleCharName() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("F");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Name, t.getType());
        assertEquals("F",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(1,t.getTo());
        assertFalse(et.hasNext());
    }


    @Test
    public void simpleSingleOperator() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("+");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Operator, t.getType());
        assertEquals("+",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(1,t.getTo());
        assertFalse(et.hasNext());
    }


    @Test
    public void simpleMultiCharacterOperator() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("!==");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Operator, t.getType());
        assertEquals("!==",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(3,t.getTo());
        assertFalse(et.hasNext());
    }


    @Test
    public void simpleString() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("\"Foo\"");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.String,t.getType());
        assertEquals("Foo",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(5,t.getTo());
        assertFalse(et.hasNext());
    }

    @Test
    public void simpleEscapedString() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("\"Foo\\\" \\\\ \\t\\n\"");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.String,t.getType());
        assertEquals("Foo\" \\ \t\n",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(15,t.getTo());
        assertFalse(et.hasNext());
    }

    @Test
    public void simpleOperatorWhitespaceIgnored() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("  !==  \n");
        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Operator, t.getType());
        assertEquals("!==",t.getValue());
        assertEquals(2,t.getFrom());
        assertEquals(5,t.getTo());
        assertFalse(et.hasNext());
    }


    @Test
    public void compoundDecimals() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("3.33e2+6.022e-1");

        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Number,t.getType());
        assertEquals("3.33e2",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(6,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals("+",t.getValue());
        assertEquals(6,t.getFrom());
        assertEquals(7,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Number,t.getType());
        assertEquals("6.022e-1",t.getValue());
        assertEquals(7,t.getFrom());
        assertEquals(15,t.getTo());

        assertFalse(et.hasNext());
    }


    @Test
    public void complexAlgebritic() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("3*x+2");

        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Number,t.getType());
        assertEquals("3",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(1,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals("*",t.getValue());
        assertEquals(1,t.getFrom());
        assertEquals(2,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Name,t.getType());
        assertEquals("x",t.getValue());
        assertEquals(2,t.getFrom());
        assertEquals(3,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals("+",t.getValue());
        assertEquals(3,t.getFrom());
        assertEquals(4,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Number,t.getType());
        assertEquals("2",t.getValue());
        assertEquals(4,t.getFrom());
        assertEquals(5,t.getTo());

        assertFalse(et.hasNext());
    }


    @Test
    public void complexDistanceFormula() throws ParseException{
        ExpressionTokenizer et = new ExpressionTokenizer("SQRT((x1-x2)^2+(y1-y2)^2)");

        assertTrue(et.hasNext());
        Token t = et.next();
        assertEquals(Token.TokenType.Name,t.getType());
        assertEquals("SQRT",t.getValue());
        assertEquals(0,t.getFrom());
        assertEquals(4,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals("(",t.getValue());
        assertEquals(4,t.getFrom());
        assertEquals(5,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals("(",t.getValue());
        assertEquals(5,t.getFrom());
        assertEquals(6,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Name,t.getType());
        assertEquals("x1",t.getValue());
        assertEquals(6,t.getFrom());
        assertEquals(8,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals("-",t.getValue());
        assertEquals(8,t.getFrom());
        assertEquals(9,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Name,t.getType());
        assertEquals("x2",t.getValue());
        assertEquals(9,t.getFrom());
        assertEquals(11,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals(")",t.getValue());
        assertEquals(11,t.getFrom());
        assertEquals(12,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals("^",t.getValue());
        assertEquals(12,t.getFrom());
        assertEquals(13,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Number,t.getType());
        assertEquals("2",t.getValue());
        assertEquals(13,t.getFrom());
        assertEquals(14,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals("+",t.getValue());
        assertEquals(14,t.getFrom());
        assertEquals(15,t.getTo());


        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals("(",t.getValue());
        assertEquals(15,t.getFrom());
        assertEquals(16,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Name,t.getType());
        assertEquals("y1",t.getValue());
        assertEquals(16,t.getFrom());
        assertEquals(18,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals("-",t.getValue());
        assertEquals(18,t.getFrom());
        assertEquals(19,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Name,t.getType());
        assertEquals("y2",t.getValue());
        assertEquals(19,t.getFrom());
        assertEquals(21,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals(")",t.getValue());
        assertEquals(21,t.getFrom());
        assertEquals(22,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals("^",t.getValue());
        assertEquals(22,t.getFrom());
        assertEquals(23,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Number,t.getType());
        assertEquals("2",t.getValue());
        assertEquals(23,t.getFrom());
        assertEquals(24,t.getTo());

        assertTrue(et.hasNext());
        t = et.next();
        assertEquals(Token.TokenType.Operator,t.getType());
        assertEquals(")",t.getValue());
        assertEquals(24,t.getFrom());
        assertEquals(25,t.getTo());

        assertFalse(et.hasNext());
    }


}