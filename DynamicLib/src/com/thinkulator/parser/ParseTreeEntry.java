/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package com.thinkulator.parser;

import java.util.ArrayList;

/**
 *
 * @author hack
 */
public class ParseTreeEntry {
    private String _id;
    private Token _token;
    private Symbol _symbol;

        /** Branches of the tree
     * 
     * For binary operators - left is first, right is second
     */
    private ArrayList<ParseTreeEntry> _branches;
    
    public ParseTreeEntry(String id, Token token, Symbol symbol) {
        this._token = token;
        this._symbol = symbol;
        this._id = id;
        _branches = new ArrayList<ParseTreeEntry>();
    }

    public ParseTreeEntry(String id, Symbol symbol) {
        this(id,null,symbol);
    }

    /**
     * @return the _id
     */
    public String getId() {
        return _id;
    }

    /**
     * @return the _token
     */
    public Token getToken() {
        return _token;
    }

    /**
     * @return the _branches
     */
    public ArrayList<ParseTreeEntry> getBranches() {
        return _branches;
    }

    public ParseTreeEntry getBranch(int i){
        return _branches.get(i);
    }
    
    public int getNumBranches(){
        return _branches.size();
    }


    public void addBranch(ParseTreeEntry pte){
        _branches.add(pte);
    }

    public void setBranch(int ord,ParseTreeEntry pte){
        while(ord >= _branches.size()){
            _branches.add(null);
        }
        _branches.set(ord,pte);
    }
    /**
     * @return the _symbol
     */
    public Symbol getSymbol() {
        return _symbol;
    }
}
