/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package com.thinkulator.parser;

import com.thinkulator.parser.Token.TokenType;
import java.text.ParseException;

/**  Formula parser similar to Excel functions (but not the same as it)
 * Mostly a port of http://javascript.crockford.com/tdop/tokens.js to java,
 * with some translations to this use case, instead of javascript.
 *
 * @author eric@bootseg.com
 */
public class ExpressionTokenizer {

    private String _expression;
    private String _prefixTokens;
    private String _suffixTokens;
    
    private int _curPos;
    private int _from;

    public ExpressionTokenizer(String expression,String prefixTokens,String suffixTokens){
        _expression = expression;
        _prefixTokens = prefixTokens;
        _suffixTokens = suffixTokens;
        _curPos = 0;
        _from = 0;
    }

    public ExpressionTokenizer(String expression){
        //TODO: Validate that these are the values we want as operators.
        this(expression,"=<>!+-*&|/%^","=<>&|");
    }

    public boolean hasNext() throws ParseException {
        //early check to see if we're definitely at the end
        if(_curPos >=_expression.length()){
            return false;
        }
        
        //more thorough check to ignore whitespace.
        char cur = _expression.charAt(_curPos);
        while(
           (       cur == ' '
                || cur == '\t'
                || cur == '\n'
                || cur == '\r')
            && hasNextChar()){
            advance();
            if(hasNextChar()){
                cur = getCurChar();
            }
        }

        return _curPos <_expression.length();
    }

    public Token next() throws ParseException{
        char cur = _expression.charAt(_curPos);
        
        //ignore whitespace between tokens
        while(
           (       cur == ' '
                || cur == '\t'
                || cur == '\n'
                || cur == '\r')
            && hasNextChar()){
            advance();
            if(hasNextChar()){
                cur = getCurChar();
            }
        }

        //check for some kind of name
        if((cur >= 'a' && cur <= 'z') || (cur >= 'A' && cur <= 'Z') || cur == '_') {
            _from = _curPos;

            //and find the rest of it.
            advance();
            while( hasNextChar()
                && (((cur = getCurChar()) >= 'a' && cur <= 'z')
                  ||(cur >= 'A' && cur <= 'Z')
                  ||(cur >= '0' && cur <= '9')
                  || cur == '_'
                  || cur == '.' )){
                advance();
            }
            return new Token(TokenType.Name,_expression,_from,_curPos);

        //and now numbers
        }else if(cur >= '0' && cur <= '9') {
            _from = _curPos;

            //find the integer portion
            advance();
            while(hasNextChar() &&
              ((cur = getCurChar()) >= '0' && cur <= '9')){
                advance();
            }

            //does it have a decimal fraction?
            if(cur == '.'){
                advance();
                if(!hasNextChar()){
                    throw new ParseException("Expected numbers after decimal point", _curPos);
                }
                while(hasNextChar() &&
                  ((cur = getCurChar()) >= '0' && cur <= '9')){
                    advance();
                }
            }

            //what about an exponent?  (6.02214179e23) or (2.1121e-10)
            if(cur == 'e' || cur == 'E'){
                advance();
                cur = getCurChar();
                
                //optional sign
                if(cur == '-' || cur == '+'){
                    advance();
                    cur = getCurChar();
                }
                if(!(cur >= '0' && cur <= '9')){
                    throw new ParseException("Expected number in exponent", _curPos);
                }
                while(hasNextChar() &&
                  ((cur = getCurChar()) >= '0' && cur <= '9' )){
                    advance();
                }
            }

            //and make sure that no one did anything silly like cram a letter at the end we don't expect (like trying to say it's in binary, or a long using C notation
            if ((cur >= 'a' && cur <= 'z') || (cur >= 'A' && cur <= 'Z')) {
                throw new ParseException("Unexpected letter at end of number", _curPos);
            }

            //TODO: test that the number is actually a parseable number, once we fully decide what that is, and how we're gonna do it.

            return new Token(TokenType.Number,_expression,_from,_curPos);
        
        //strings are pretty awesome too, allow both ' and " for string markers
        //excel only allows ", ' is for references, but we don't allow spaces in variable names, so no need for quoted identifiers
        }else if(cur == '"' || cur == '\''){
            _from = _curPos; 
            char stringTerminatorChar = cur;
            advance(); //Strings don't include the enclosed quotes
            cur = getCurChar();
            StringBuilder sb = new StringBuilder();
            
            while(true){
                //let's catch some obvious characters that would imply an unclosed string, so we can error out
                //this includes newlines, nulls, etc
                if(cur < ' '){
                    throw new ParseException("Unclosed string, expected ["+Character.toString(stringTerminatorChar)+"]", _curPos);
                }

                if(cur == stringTerminatorChar){
                    advance();//advance past the terminator characters
                    return new Token(TokenType.String,_expression,sb.toString(),_from,_curPos);
                }

                //TODO: add escape sequence parsing
                if(cur == '\\'){
                    advance();
                    cur = getCurChar();
                    switch(cur){
                      /*  case 'n':
                            cur = '\n';
                            break;
                        case 't':
                            cur = '\t';
                            break;*/
                        case '\\':
                            cur = '\\';
                            break;
                        /*case '0':  //TODO: Do I want to allow null?
                            cur = '\0';
                            break;
                        case '"':  //currently ' is not allowed, as it will escape the javascript wrappings we use
                            cur = '"';
                            break;*/
                        default:
                            throw new ParseException("Unexpected escape sequence character ["+Character.toString(cur)+"]", _curPos);
                    }
                }

                //add to the output string and advance
                sb.append(cur);
                advance();
                cur = getCurChar();
            }

        //and now multi-character operators
        }else if(_prefixTokens.indexOf(cur) >= 0){
            _from = _curPos;
            advance();
            //keep looking till we see a suffix character
            while(hasNextChar() &&
                  _suffixTokens.indexOf(getCurChar()) >= 0){
                advance();
            }
            return new Token(TokenType.Operator,_expression,_from,_curPos);
        }else{
            _from = _curPos;
            advance();
            return new Token(TokenType.Operator,_expression,_from,_curPos);
        }
    }

    public void remove() {
        throw new UnsupportedOperationException("Expression tokenizer does not support removing tokens.");
    }


    
    private boolean hasNextChar(){
        return _curPos < _expression.length();
    }

    private void advance(){
        _curPos++;
    }
    
    private char getCurChar() throws ParseException{
        if(_curPos  > _expression.length()){
            throw new ParseException("Premature end of expression", _curPos);
        }
        return _expression.charAt(_curPos);
    }
    
}
