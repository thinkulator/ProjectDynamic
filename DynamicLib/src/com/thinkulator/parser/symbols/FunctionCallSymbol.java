/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package com.thinkulator.parser.symbols;

import com.thinkulator.parser.ExpressionTreeBuilder;
import com.thinkulator.parser.ExpressionTreeBuilderException;
import com.thinkulator.parser.ParseTreeEntry;
import java.text.ParseException;

/**
 *
 * @author hack
 */
public class FunctionCallSymbol extends BasicSymbol {
    public FunctionCallSymbol(String id,int bindingPriority){
        super(id,bindingPriority);
    }

    @Override
    public ParseTreeEntry getNud(ParseTreeEntry self, ExpressionTreeBuilder builder) throws ExpressionTreeBuilderException, ParseException {
        builder.advance("(");
        if(!")".equals(builder.getCurrentEntry().getId())){
            while(true){
                self.addBranch(builder.expression(0));

                if(!(")".equals(builder.getCurrentEntry().getId())
                   || ",".equals(builder.getCurrentEntry().getId()))){
                    throw new ExpressionTreeBuilderException("Expected [(] or [,]",builder.getCurrentEntry().getToken());
                }
                if(")".equals(builder.getCurrentEntry().getId())){
                    break;
                }
                builder.advance(",");
            }
        }
        builder.advance(")");
        return self;
    }


}
