/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package com.thinkulator.compiler;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hack
 */
public class CompilationResult {
    private String _calculationSource;
    private List<String> _references;

    public CompilationResult(String language) {
        _references = new ArrayList<String>();
    }

    public String getCalculationSource() {
        return _calculationSource;
    }

    public void setCalculationSource(String calculationJavascript) {
        _calculationSource = calculationJavascript;
    }

    public void addReference(String fieldName){
        _references.add(fieldName);
    }

    public List<String> getReferences() {
        return _references;
    }
}
