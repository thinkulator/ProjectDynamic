


//lifted from http://alcor.concordia.ca/~gpkatch/gdate-c.html
void dtf(struct sdate *pd,long d) { /* convert day number to y,m,d format */
  long y, ddd, mm, dd, mi;

  y = (10000*d + 14780)/3652425;
  ddd = d - (y*365 + y/4 - y/100 + y/400);
  if (ddd < 0) {
    y--;
    ddd = d - (y*365 + y/4 - y/100 + y/400);
    }
  mi = (52 + 100*ddd)/3060;
  pd->y = y + (mi + 2)/12;
  pd->m = (mi + 2)%12 + 1;
  pd->d = ddd - (mi*306 + 5)/10 + 1;
}


void convertTime(struct sdate *da,unsigned long epoch,uint16_t milliseconds){
  //figure out the day of the week
      long dayOfWeek = 3;
      long daysSinceEpoch = epoch/86400L;
      dayOfWeek = (daysSinceEpoch+4)%7; //January 1st 1970 was a Thursday
      da->dow = dayOfWeek;

  //now get the year, month, and day, and store them in the structure
      dtf(da,(daysSinceEpoch)+724046); //  number of days from Year 0 to 1970

  //and the hours, minutes and seconds
      da->hours = ((epoch  % 86400L) / 3600); // (86400 equals secs per day)
      da->minutes = ((epoch  % 3600) / 60); // (3600 equals secs per minute)
      da->seconds = (epoch %60); // finally the seconds
      da->milliseconds = milliseconds;
      da->epoch = epoch;
}


char * getDowName(int dow){

    switch(dow){
        case 0:
          return "Sunday";
        case 1:
          return "Monday";
        case 2:
          return "Tuesday";
        case 3:
          return "Wednesday";
        case 4:
          return "Thursday";
        case 5:
          return "Friday";
        case 6:
          return "Saturday";
      }
  return "Invalid DOW";
}

char * formatTime(char *buffer,int buffer_length,unsigned long epoch,uint16_t milli){
      struct sdate da;

      convertTime(&da,epoch,milli);

      snprintf(buffer,buffer_length,"%s %04d-%02d-%02d %02d:%02d:%02d.%03d",getDowName(da.dow),da.y,da.m,da.d,da.hours,da.minutes,da.seconds,da.milliseconds);

      return buffer;
}




