struct value_store {
  struct value_store *left;
  struct value_store *right;
  char *name;
  char *value;
};

char * get_stored_value(char *name,struct value_store *tree);
char * put_stored_value(char *name, char *value,struct value_store *tree);
struct value_store *new_value_store(char *name,char *value);
