/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.compiler;

import com.thinkulator.parser.ParseTreeEntry;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author hack
 */
public class ExpressionCLangCompiler {
    private Map<String,FunctionCompiler> _functions;

    public ExpressionCLangCompiler(){
        _functions = new TreeMap<String,FunctionCompiler>(String.CASE_INSENSITIVE_ORDER);

        /** Don't forget to add an entry to ExpreessionTreeBuilder.java */
        _functions.put("UPTIME",new FunctionCompiler() {
            @RequiredBranches(0)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("ptr->ltostr(ptr->millis())");
            }
        });

        _functions.put("IF",new FunctionCompiler() {
            @RequiredBranches(3)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(ptr->isTrue(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(",1)?("); //the ,1 is to free the parameter when done with it.
                compiler.compile(pte.getBranches().get(1),sb,result);
                sb.append("):(");
                compiler.compile(pte.getBranches().get(2),sb,result);
                sb.append("))");
            }
        });
        
        /*_functions.put("SQRT",new FunctionCompiler() {
            @RequiredBranches(1)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                //TODO: Find/build a BigDecimal version of Square root...
                sb.append("String(Math.sqrt(Number(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(")))");
            }
        });
        
        _functions.put("LEFT",new FunctionCompiler() {
            @RequiredBranches(2)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").substring(0,Number(");
                compiler.compile(pte.getBranches().get(1),sb,result);
                sb.append(")))");
            }
        });
        _functions.put("MID",new FunctionCompiler() {
            @RequiredBranches(3)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").substr(Number(");
                compiler.compile(pte.getBranches().get(1),sb,result);
                sb.append(")-1,Number(");
                compiler.compile(pte.getBranches().get(2),sb,result);
                sb.append(")))");
            }
        });
        _functions.put("RIGHT",new FunctionCompiler() {
            @RequiredBranches(2)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").substring(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").length-Number(");
                compiler.compile(pte.getBranches().get(1),sb,result);
                sb.append(")))");
            }
        });
        _functions.put("LEN",new FunctionCompiler() {
            @RequiredBranches(1)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").length)");
            }
        });
        _functions.put("TRIM",new FunctionCompiler() {
            @RequiredBranches(1)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").trim())");
            }
        });
        _functions.put("LOWER",new FunctionCompiler() {
            @RequiredBranches(1)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").toLowerCase())");
            }
        });
        _functions.put("UPPER",new FunctionCompiler() {
            @RequiredBranches(1)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").toUpperCase())");
            }
        });
        _functions.put("SUM",new FunctionCompiler() {
            @RequiredBranches(value=1,min=true)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(function(){var va=[];");
                List<ParseTreeEntry> branches = pte.getBranches();
                sb.append("va=va.concat(");
                boolean first=true;
                for(ParseTreeEntry pti:branches){
                    if(first){
                        first=false;
                    }else{
                        sb.append(",");
                    }
                    compiler.compile(pti,sb,result);
                }
                sb.append(");");
                sb.append("var r=new BigDecimal('0');");
                sb.append("for(var i=0;i<va.length;i++){");
                sb.append("r=r.add(new BigDecimal(String(va[i])))");
                sb.append("}");
                sb.append("return String(r);})()");
            }
        });
        _functions.put("COUNT",new FunctionCompiler() {
            @RequiredBranches(value=1,min=true)
            public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(function(){var va=[];");
                List<ParseTreeEntry> branches = pte.getBranches();
                sb.append("va=va.concat(");
                boolean first=true;
                for(ParseTreeEntry pti:branches){
                    if(first){
                        first=false;
                    }else{
                        sb.append(",");
                    }
                    compiler.compile(pti,sb,result);
                }
                sb.append(");");
                sb.append("var r=new BigDecimal('0');");
                sb.append("for(var i=0;i<va.length;i++){");
                sb.append("if($.isNumeric(va[i])){");
                sb.append("r=r.add(new BigDecimal('1'))");
                sb.append("}}");
                sb.append("return String(r);})()");
            }
        });*/
    }

    public CompilationResult compile(ParseTreeEntry pte){
        CompilationResult result = new CompilationResult("C");

        StringBuilder sb = new StringBuilder();
        compile(pte,sb,result);
        result.setCalculationSource(sb.toString());
        
        return result;
    }

    protected void compile(ParseTreeEntry pte,StringBuilder sb,CompilationResult result){
        String symbolID = pte.getSymbol().getID();
        if( "-".equals(symbolID) && pte.getNumBranches() == 1){
            //This is a negation, not a subtraction
           sb.append("ptr->do_negate(");
           compile(pte.getBranches().get(0),sb,result);
           sb.append(")");
                

            //we'll start w/ just supporting numbers.  Later we'll move on to text too
        }else if(pte.getNumBranches() == 2){
            
            if("+".equals(symbolID)){
                sb.append("ptr->do_add(");
            }else if("-".equals(symbolID)){
                sb.append("ptr->do_sub(");
            }else if("*".equals(symbolID)){
                sb.append("ptr->do_mul(");
            }else if("/".equals(symbolID)){
                sb.append("ptr->do_div(");
            }else if("%".equals(symbolID)){
                sb.append("ptr->do_mod(");
            }else if("^".equals(symbolID)
                || "POWER".equalsIgnoreCase(symbolID)){
                sb.append("ptr->do_pow(");
            }else if("AND".equals(symbolID)){
                sb.append("ptr->do_and(");
            }else if("OR".equals(symbolID)){
                sb.append("ptr->do_or(");
            }else if("<".equals(symbolID) ||
                    "<=".equals(symbolID) ||
                    "=".equals(symbolID)  ||
                    ">=".equals(symbolID) ||
                    ">".equals(symbolID)){
                sb.append("ptr->do_compare(");
            }else{
                throw new RuntimeException("Not sure what do do with "+symbolID);
            }
                
            compile(pte.getBranches().get(0),sb,result);
            sb.append(",");
            compile(pte.getBranches().get(1),sb,result);
            
            switch(symbolID){
                case "<":
                    sb.append(",COMPARE_LT)");
                    break;
                case ">":
                    sb.append(",COMPARE_GT)");
                    break;
                case "<=":
                    sb.append(",COMPARE_LTE)");
                    break;
                case ">=":
                    sb.append(",COMPARE_GTE)");
                    break;
                case "=":
                    sb.append(",COMPARE_E)");
                    break;
                default:
                    sb.append(")");
             }
        }else if("NOT".equals(symbolID)){
            if(pte.getNumBranches() != 1){
                //TODO: Exception
            }
            sb.append("(ptr->do_not(");
            compile(pte.getBranches().get(0),sb,result);
            sb.append("))");
        }else if("true".equals(symbolID)){
            sb.append("ptr->FP(\"true\")");
        }else if("false".equals(symbolID)){
            sb.append("ptr->FP(\"false\")");
        }else if("(literal)".equals(pte.getSymbol().getID())){
            sb.append("ptr->FP(\"");
            //SECURITY - Are the Java string rules and C rules EXACTLY the same?
            sb.append(StringEscapeUtils.escapeJava(pte.getToken().getValue()));
            sb.append("\")");
        }else if("(name)".equals(pte.getSymbol().getID())){
            sb.append("ptr->do_get_stored(ptr->FP(\"");  //FV is a reference to adder.js->findValue
            //SECURITY - Are the Java string rules and C rules EXACTLY the same?
            sb.append(StringEscapeUtils.escapeJava(pte.getToken().getValue()));
            sb.append("\"))");
            result.addReference(pte.getToken().getValue());
        }else{
            //call a registered handler for a function
            if(_functions.containsKey(pte.getSymbol().getID())){
                try {
                    int [] numRequiredBranches = null;
                    boolean isMin = false;
                    //check for the annotations used to mark requirements and other information
                    FunctionCompiler fc = _functions.get(pte.getSymbol().getID());


                    Method m = fc.getClass().getMethod("compile", ExpressionCLangCompiler.class, ParseTreeEntry.class, StringBuilder.class, CompilationResult.class);
                    if (m.isAnnotationPresent(RequiredBranches.class)) {
                        RequiredBranches c = m.getAnnotation(RequiredBranches.class);
                        isMin = c.min();
                        numRequiredBranches = c.value();

                    }

                    if(numRequiredBranches != null){
                        boolean okay=false;
                        if(isMin){
                            if(pte.getNumBranches() >= numRequiredBranches[0]){
                                okay=true;
                            }
                        }else{
                            for(int i=0;i<numRequiredBranches.length;i++){
                                if(pte.getNumBranches() == numRequiredBranches[i]){
                                    okay=true;
                                }
                            }
                        }
                        if(!okay){
                            //TODO:throw an exception about not enough parameters
                        }
                    }
                    _functions.get(pte.getSymbol().getID()).compile(this, pte, sb, result);

                } catch (NoSuchMethodException ex) {
                    throw new IllegalArgumentException("FunctionCompilers must have the compile method...", ex);
                } catch (SecurityException ex) {
                    throw new IllegalArgumentException("FunctionCompilers must have the compile method declared public...", ex);
                }
            }
        }
    }

    public static interface FunctionCompiler{
        public void compile(ExpressionCLangCompiler compiler,ParseTreeEntry pte,StringBuilder sb,CompilationResult result);
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public static @interface RequiredBranches{
        public int [] value();
        public boolean min() default false;
    }
}
