/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.parser.symbols;

import com.thinkulator.parser.ExpressionTreeBuilder;
import com.thinkulator.parser.ExpressionTreeBuilderException;
import com.thinkulator.parser.ParseTreeEntry;
import java.text.ParseException;

/**
 *
 * @author hack
 */
public class FunctionCallSymbol extends BasicSymbol {
    public FunctionCallSymbol(String id,int bindingPriority){
        super(id,bindingPriority);
    }

    @Override
    public ParseTreeEntry getNud(ParseTreeEntry self, ExpressionTreeBuilder builder) throws ExpressionTreeBuilderException, ParseException {
        builder.advance("(");
        if(!")".equals(builder.getCurrentEntry().getId())){
            while(true){
                self.addBranch(builder.expression(0));

                if(!(")".equals(builder.getCurrentEntry().getId())
                   || ",".equals(builder.getCurrentEntry().getId()))){
                    throw new ExpressionTreeBuilderException("Expected [(] or [,]",builder.getCurrentEntry().getToken());
                }
                if(")".equals(builder.getCurrentEntry().getId())){
                    break;
                }
                builder.advance(",");
            }
        }
        builder.advance(")");
        return self;
    }


}
