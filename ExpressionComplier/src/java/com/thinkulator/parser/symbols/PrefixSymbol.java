/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.parser.symbols;

import com.thinkulator.parser.ExpressionTreeBuilder;
import com.thinkulator.parser.ExpressionTreeBuilderException;
import com.thinkulator.parser.ParseTreeEntry;
import java.text.ParseException;

/**
 *
 * @author hack
 */
public class PrefixSymbol extends BasicSymbol {
    public PrefixSymbol(String id,int bindingPriority){
        super(id,bindingPriority);
    }

    @Override
    public ParseTreeEntry getNud(ParseTreeEntry self, ExpressionTreeBuilder builder) throws ExpressionTreeBuilderException, ParseException {
        self.setBranch(0, builder.expression(70));
        return self;
    }


}
