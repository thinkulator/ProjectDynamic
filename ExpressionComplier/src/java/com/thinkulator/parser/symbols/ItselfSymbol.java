/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.parser.symbols;

import com.thinkulator.parser.ExpressionTreeBuilder;
import com.thinkulator.parser.ExpressionTreeBuilderException;
import com.thinkulator.parser.ParseTreeEntry;
import com.thinkulator.parser.Symbol;

/**
 *
 * @author hack
 */
public class ItselfSymbol extends Symbol{
    public ItselfSymbol(String id,int bindingParameter){
        super(id,bindingParameter);
    }


    @Override
    public ParseTreeEntry getNud(ParseTreeEntry self, ExpressionTreeBuilder builder) throws ExpressionTreeBuilderException {
        return self;
    }

    @Override
    public ParseTreeEntry getLed(ParseTreeEntry self, ExpressionTreeBuilder builder,ParseTreeEntry left) throws ExpressionTreeBuilderException {
        throw new ExpressionTreeBuilderException("Missing operator");
    }

}
