/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.parser;

import java.text.ParseException;

/**
 *
 * @author hack
 */
public abstract class Symbol {
    private String _id;
    private int    _leftBindingPriority = 0;
    
    public Symbol(String id,int leftBindingPriority){
        _id = id;
        _leftBindingPriority = leftBindingPriority;
    }

    public Symbol(String id){
        this(id,0);
    }

    public String getID(){
        return _id;
    }

    public int getLeftBindingPriority() {
        return _leftBindingPriority;
    }

    public abstract ParseTreeEntry getNud(ParseTreeEntry self, ExpressionTreeBuilder builder) throws ExpressionTreeBuilderException, ParseException;
    public abstract ParseTreeEntry getLed(ParseTreeEntry self, ExpressionTreeBuilder builder,ParseTreeEntry left) throws ExpressionTreeBuilderException, ParseException;


}
