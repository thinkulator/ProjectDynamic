#include <stdint.h>
#include "number.h"
#include <stdlib.h>



struct bc_ptr {
	void (*bc_init_numbers)(void); 
	void (*bc_free_numbers)(void);
	bc_num (*bc_new_num)(int length, int scale);
	void (*bc_free_num)(bc_num *num);

	bc_num (*bc_copy_num)(bc_num num);

	void (*bc_init_num)(bc_num *num);

	void (*bc_str2num)(bc_num *num, const char *str, int scale);

	char *(*bc_num2str)(bc_num num);

	void (*bc_int2num)(bc_num *num, int val);

	long (*bc_num2long)(bc_num num);

	int (*bc_compare)(bc_num n1, bc_num n2);

	char (*bc_is_zero)(bc_num num);

	char (*bc_is_near_zero)(bc_num num, int scale);

	char (*bc_is_neg)(bc_num num);

	void (*bc_add)(bc_num n1, bc_num n2, bc_num *result, int scale_min);

	void (*bc_sub)(bc_num n1, bc_num n2, bc_num *result, int scale_min);

	void (*bc_multiply)(bc_num n1, bc_num n2, bc_num *prod, int scale);

	int (*bc_divide)(bc_num n1, bc_num n2, bc_num *quot, int scale);

	int (*bc_modulo)(bc_num num1, bc_num num2, bc_num *result,
			   int scale);

	int (*bc_divmod)(bc_num num1, bc_num num2, bc_num *quot,
				   bc_num *rem, int scale);

	int (*bc_raisemod)(bc_num base, bc_num expo, bc_num mod,
				     bc_num *result, int scale);

	void (*bc_raise)(bc_num num1, bc_num num2, bc_num *result,
				   int scale);

	int (*bc_sqrt)(bc_num *num, int scale);

	void (*bc_out_num) (bc_num num, int o_base, void (* out_char)(int),
				     int leading_zero);

  void * (*malloc)(size_t size);
  void   (*free)(void *ptr);


};


static char * do_mod(const char *left, const char *right,struct bc_ptr *ptr);

static char *                  /* addr of terminating null */
ltostr (
    char *str,          /* output string */
    long val           /* value to be converted */
    )      /* conversion base       */
{

    //if the string pointer has not been passed, allocate a buffer.
    long q,r;

    if (val < 0)    *str++ = '-';

    r = val%10;
    q = val/10;

    /* output digits of val/base first */

    if (q > 0)  str = ltostr (str,q);

    /* output last digit */
    
    *str++ = '0'+r;
    *str   = '\0';
    return str;
}


char  * doit(unsigned long now,struct bc_ptr *ptr){
        char *l_str = (char *)ptr->malloc(16); //only really need 11, but oh well.
        l_str[0] = 'H';
        l_str[1] = 'I';
        l_str[2] = 0;
        l_str[3] = 0;
        l_str[4] = 0;
        l_str[5] = 0;
        l_str[6] = 0;
        l_str[7] = 0;
        l_str[8] = 0;
        l_str[9] = 0;
        l_str[10] = 0;
        l_str[11] = 0;
        l_str[12] = 0;
        l_str[13] = 0;
        l_str[14] = 0;
        l_str[15] = 0;
 
	ltostr(l_str,9999);
	const static char *r_str = "1000";

        char *ret = do_mod(l_str,r_str,ptr);
        ptr->free(l_str);

        return ret;
}



static char * do_mod(const char *left, const char *right,struct bc_ptr *ptr){
	char *res_str;
	
	bc_num l = ptr->bc_new_num(10,10);
	bc_num r = ptr->bc_new_num(10,10);

	bc_num res = ptr->bc_new_num(10,10);

	ptr->bc_str2num(&l,left,0);
	ptr->bc_str2num(&r,right,0);

	ptr->bc_modulo(l,r,&res,1);
     
	ptr->bc_free_num(&l);
	ptr->bc_free_num(&r);

	res_str = ptr->bc_num2str(res);

	ptr->bc_free_num(&res);
	return res_str;
}

void main(){
   printf("Res: %s\n",doit(
}
