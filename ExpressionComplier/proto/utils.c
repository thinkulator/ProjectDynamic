#include <stdint.h>
#include <stdlib.h>
#include "bcconfig2.h"
#include "utils.h"



static char *                  /* addr of terminating null */
ltostr (
    char *str,          /* output string */
    long val           /* value to be converted */
    )
{

    //if the string pointer has not been passed, allocate a buffer.
    long q,r;

    if (val < 0)    *str++ = '-';

    r = val%10;
    q = val/10;

    /* output digits of val/base first */

    if (q > 0)  str = ltostr (str,q);

    /* output last digit */
    
    *str++ = '0'+r;
    *str   = '\0';
    return str;
}


static char * do_mod(char *left, char *right){
	char *res_str;
	
	bc_num l = bc_new_num(10,10);
	bc_num r = bc_new_num(10,10);

	bc_num res = bc_new_num(0,0);

	bc_str2num(&l,left,10);
	bc_str2num(&r,right,10);


	bc_modulo(l,r,&res,0);
     
	bc_free_num(&l);
	bc_free_num(&r);

	res_str = bc_num2str(res);

	bc_free_num(&res);

        free(left);
        free(right);

	return res_str;
}


static char * do_add(char *left, char *right){
	char *res_str;
	
	bc_num l = bc_new_num(10,10);
	bc_num r = bc_new_num(10,10);

	bc_num res = bc_new_num(0,0);

	bc_str2num(&l,left,10);
	bc_str2num(&r,right,10);


	bc_add(l,r,&res,0);
     
	bc_free_num(&l);
	bc_free_num(&r);

	res_str = bc_num2str(res);

	bc_free_num(&res);
        free(left);
        free(right);

	return res_str;
}

static char * do_sub(char *left, char *right){
	char *res_str;
	
	bc_num l = bc_new_num(10,10);
	bc_num r = bc_new_num(10,10);

	bc_num res = bc_new_num(0,0);

	bc_str2num(&l,left,10);
	bc_str2num(&r,right,10);


	bc_sub(l,r,&res,0);
     
	bc_free_num(&l);
	bc_free_num(&r);

	res_str = bc_num2str(res);

	bc_free_num(&res);
        free(left);
        free(right);

	return res_str;
}

static char * do_mul(char *left, char *right){
	char *res_str;
	
	bc_num l = bc_new_num(10,10);
	bc_num r = bc_new_num(10,10);

	bc_num res = bc_new_num(0,0);

	bc_str2num(&l,left,10);
	bc_str2num(&r,right,10);


	bc_multiply(l,r,&res,0);
     
	bc_free_num(&l);
	bc_free_num(&r);

	res_str = bc_num2str(res);

	bc_free_num(&res);
        free(left);
        free(right);
	return res_str;
}


static char * do_compare(char *left, char *right,char type){
	char *res_str = malloc(4);
	
	bc_num l = bc_new_num(10,10);
	bc_num r = bc_new_num(10,10);

	bc_str2num(&l,left,10);
	bc_str2num(&r,right,10);

        int res = bc_compare(l,r);
        res_str[1] = 0;

        if((type == COMPARE_LT && res < 0)   ||
           (type == COMPARE_GT && res >0)    ||
           (type == COMPARE_LTE && res <= 0) ||
           (type == COMPARE_GTE && res >= 0) ||
           (type == COMPARE_E && res == 0)){
                res_str[0] = '1';
	}else{
                res_str[0] = '0';
	}
        bc_free_num(&l);
	bc_free_num(&r);

        free(left);
        free(right);

	return res_str;
}

