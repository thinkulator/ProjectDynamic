#include "utils.h"

char  * __attribute__ ((noinline)) __attribute__((section(".entry"))) _start(unsigned long now,struct operations_ptr *ptr){
        return ptr->do_and(
		ptr->do_compare(
			ptr->do_mod(
				ptr->ltostr(now),
				ptr->ltostr(4000)
			),
			ptr->ltostr(1000),
			COMPARE_GT
		),
		ptr->do_compare(
			ptr->do_mod(
				ptr->ltostr(now),
				ptr->ltostr(4000)
			),
			ptr->ltostr(2000),
			COMPARE_LT
		)
	);

}


