var net = require('net');
var fs = require('fs');


// Keep track of the chat clients
var clients = [];

var fn = function(eventType, filename){
	
	if(eventType == 'change'){
		for(var i=0;i<clients.length;i++){
			clients[i].write(filename[3]);
		}
		console.log('File Event',eventType,filename[3],filename);
	}
};


var data = {
	'out0':fs.watch('/var/www/html/out0.hex',fn),
	'out1':fs.watch('/var/www/html/out1.hex',fn),
	'out2':fs.watch('/var/www/html/out2.hex',fn),
	'out3':fs.watch('/var/www/html/out3.hex',fn),
	'out4':fs.watch('/var/www/html/out4.hex',fn)
};


// Start a TCP Server
var server = net.createServer(function (socket) {
  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort 

  console.log('Connected',socket.name);

  // Put this new client in the list
  clients.push(socket);


  // Handle incoming messages from clients.
  socket.on('data', function (data) {
	
  });

  socket.on('error', function (err) {
	console.log(err);
  });
  // Remove the client from the list when it leaves
  socket.on('end', function () {
	clients.splice(clients.indexOf(socket), 1);
	console.log(socket.name," disconnected.");
  });
  
}).listen(5000);

server.on('error',function(err){
   console.log(err);
});

// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 5000\n");
