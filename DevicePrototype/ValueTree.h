/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

struct value_store {
  struct value_store *left;
  struct value_store *right;
  char *name;
  char *value;
};

char * get_stored_value(char *name,struct value_store *tree);
char * put_stored_value(char *name, char *value,struct value_store *tree);
struct value_store *new_value_store(char *name,char *value);



