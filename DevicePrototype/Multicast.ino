/** Copyright 2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "Multicast.h"

Multicast::Multicast()
{
    
}

int Multicast::join(uint8_t fi,uint8_t se,uint8_t th,uint8_t fo){
  #ifdef __TM4C1294NCPDT__
    ip_addr_t ipaddr;
    ip_addr_t ipgroup;
    
    ip_addr_set_any(&ipaddr); //multicast listen on all ip addresses
    IP4_ADDR(&ipgroup,fi,se,th,fo);
 
    return igmp_joingroup( &ipaddr, &ipgroup );
 #elif defined(__CC3200R1M1RGC__)
    SlSockIpMreq mreq;
    mreq.imr_multiaddr.s_addr = sl_Htonl(SL_IPV4_VAL(fi,se,th,fo));
    //mreq.imr_multiaddr.s_addr = 0;
    return sl_SetSockOpt(0, SL_IPPROTO_IP, SL_IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
 #else 
    Serial.println("Multicast not implemented for this hardware");
    #error not implemented for this hardware
 #endif  
}

int Multicast::leave(uint8_t fi,uint8_t se,uint8_t th,uint8_t fo){
  
}


