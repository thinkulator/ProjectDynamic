/** Copyright 2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifdef __TM4C1294NCPDT__
#include <driverlib/hibernate.h>
#include <driverlib/sysctl.h>
#endif

//Wifi Chips
#ifdef __CC3200R1M1RGC__
#include <driverlib/prcm.h>
#endif


#include "RTCDate.h"



RTCDate::RTCDate()
{
    _timeZoneOffset = 0;
}

void RTCDate::begin(UDP *socket)
{
   ntpUdp = socket;
   timeServer = IPAddress(104,155,144,4);

   ntpUdp->begin(_localPort);  
  
#ifdef __TM4C1294NCPDT__
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_HIBERNATE);
    HibernateEnableExpClk(0);
    HibernateClockConfig(HIBERNATE_OSC_LOWDRIVE);
    HibernateRTCEnable();
    
    HibernateRTCSet(1473982937L+12);
  
    HibernateCounterMode(HIBERNATE_COUNTER_RTC);
    
#else
    PRCMRTCInUseSet();
    //PRCMIntRegister (PRCM_INT_SLOW_CLK_CTR);
    PRCMIntEnable(PRCM_INT_SLOW_CLK_CTR);
    
#endif

    syncTime();
}

void RTCDate::getTime(uint32_t *sec,uint16_t *msec)
{
#ifdef __TM4C1294NCPDT__
    *sec=HWREG( 0x400FC000L);//read the RTC Seconds register
    *msec=((HWREG( 0x400FC028L) & 0xFFFFL)*1000)/32768;
#elif defined(__CC3200R1M1RGC__)
    PRCMRTCGet(sec, msec);
    //the CC3200 does not have a real RTC, and the msec counts 1024 for every 1000 milliseconds...
    //need to scale it
    uint32_t m = *msec;
    
    m*=100;
    m-=(m/4167)*100;  //4167 = 100*(1000/24) - rounded.
    m=m/100;

    *msec=m;
    
#else
    #error not implemented on this hardware
#endif
}

int32_t RTCDate::setTime(uint32_t *sec,uint16_t *msec){

#ifdef __TM4C1294NCPDT__
    long old_now = HWREG( 0x400FC000L);
    HibernateRTCSet(*sec);
    return old_now-(*sec);
#elif defined(__CC3200R1M1RGC__)
    //the CC3200 does not have a real RTC, and the msec counts 1024 for every 1000 milliseconds...
    //need to scale it
    uint32_t m = *msec;

    m*=100;
    m+=(m/4167)*100;  //4167 = 100*(1000/24) - rounded.
    m=m/100;

    PRCMRTCSet(*sec,m);


    //TODO: implement delta return
    return 0; 
#else
    #error not implemented on this hardware
    
#endif
}

void RTCDate::syncTime()
{
  //TODO: Add our current time and milliseconds - so we can get a more accurate time
  Serial.print("Sending NTP Packet to ");
  Serial.println(timeServer);
// set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  ntpUdp->beginPacket(timeServer, 123); //NTP requests are to port 123
  ntpUdp->write(packetBuffer,NTP_PACKET_SIZE);
  ntpUdp->endPacket();

}

void RTCDate::checkup()
{
   //nothing to do if begin has not been called yet.
   if(ntpUdp == 0){
    return;
   }
   
   if ( ntpUdp->parsePacket() ) {
      // We've received a packet, SECURITY TODO -> Check if it came from the appropriate time server
      
      //Now read the data from it
      int r = ntpUdp->read(packetBuffer,NTP_PACKET_SIZE);  // read the packet into the buffer
      if(r < NTP_PACKET_SIZE){
        //bad packet, ignore
      }else{
        //inSync = 1;

        //the timestamp starts at byte 40 of the received packet and is four bytes,
        // or two words, long.
  
        // combine the four bytes (two words) into a long integer
        // this is NTP time (seconds since Jan 1 1900):
        uint32_t NTPsecsSince1900 =  ((unsigned long)word(packetBuffer[40], packetBuffer[41]) << 16) | ((unsigned long) word(packetBuffer[42], packetBuffer[43]));
        //    Serial.print("Seconds since Jan 1 1900 = " );
        //    Serial.println(NTPsecsSince1900);
  
        // now convert NTP time into everyday time:
        // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
        const unsigned long seventyYears = 2208988800UL;
        // subtract seventy years:
        uint32_t NTPepoch = NTPsecsSince1900 - seventyYears;
  

        Serial.println("Setting time via NTP");

        uint32_t newTimeSec = NTPepoch+_timeZoneOffset;
        setTime(&newTimeSec,0);
     }  
   }

   //TODO: Checkin with the NTP Server every now and then
   //TODO: Add fractional time sync as well as epoch
   //TODO: Calculate offset and delay, and implement nudging instead of jumping (with adjustable tolerances)
   //TODO: Prevent backwards jumps(nudging is fine) without reboots.
   
   return; 
}






