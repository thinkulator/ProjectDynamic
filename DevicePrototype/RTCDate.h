/** Copyright 2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#ifndef RTCDate_VERSION

#define RTCDate_VERSION 001
#include <Udp.h>

class RTCDate {
public:
    
    RTCDate();

    void begin(UDP *socket);

    void checkup();

    void getTime(uint32_t*,uint16_t*);

    void syncTime();
    
    int32_t setTime(uint32_t*,uint16_t*);
    
private:
    time_t  _epochRTC;
    int32_t _timeZoneOffset;

    unsigned int _localPort = 123;      // local port to listen for UDP packets
    
    IPAddress timeServer;//104.155.144.4

    static const int NTP_PACKET_SIZE= 48; // NTP time stamp is in the first 48 bytes of the message
    byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

    UDP *ntpUdp;

};
#endif



