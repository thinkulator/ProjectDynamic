/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/



 char * ltostr (long val){
   char *ret = (char *)malloc(16);
   _ltostr(ret,val);
   return ret;
}


char isTrue(char *val,char do_free){
  if(val == 0){
    return 0;
  }
  
  if(((val[0] == 't' || val[0] == 'T') &&
     (val[1] == 'r' || val[1] == 'R') &&
     (val[2] == 'u' || val[2] == 'U') &&
     (val[3] == 'e' || val[3] == 'E'))
     || 
     ((val[0] == '1') &&
     (val[1] == '\0'))){
      if(do_free){
        my_free(val);
      }
    return 1;
  }
  if(do_free){
    my_free(val);
  }
  return 0;
}


char isNumber(char *val,char do_free){
  if(val == 0){
    return 0;
  }

  int index = 0;
  while(val[index] != 0){
    if( (val[index] >= '0' && val[index] <= '9') ||
        (val[index] == '.') || 
        (val[index] == '-' && index==0)){
      index++;
      continue;
    }else{
      if(do_free){
        my_free(val);
      }
      return 0;
    }
    
  }
  
  if(do_free){
    my_free(val);
  }
  return 1;
}


char * do_mod(char *left, char *right){
  char *res_str;
  
  bc_num l = bc_new_num(10,10);
  bc_num r = bc_new_num(10,10);

  bc_num res = bc_new_num(0,0);

  bc_str2num(&l,left,10);
  bc_str2num(&r,right,10);


  bc_modulo(l,r,&res,0);
     
  bc_free_num(&l);
  bc_free_num(&r);

  res_str = bc_num2str(res);

  bc_free_num(&res);

        my_free(left);
        my_free(right);

  return res_str;
}


char * do_add(char *left, char *right){
  char *res_str;

  if(isNumber(left,0) && isNumber(right,0)){
    //mathematical addition
    
    bc_num l = bc_new_num(10,10);
    bc_num r = bc_new_num(10,10);
  
    bc_num res = bc_new_num(0,0);
  
    bc_str2num(&l,left,10);
    bc_str2num(&r,right,10);
  
  
    bc_add(l,r,&res,0);
       
    bc_free_num(&l);
    bc_free_num(&r);
  
    res_str = bc_num2str(res);
  
    bc_free_num(&res);
  }else{
    //string concatenation
    int leftLen = strlen(left);
    int rightLen = strlen(right);
    
    res_str = (char *) malloc(leftLen+rightLen+1);

    memcpy(res_str,left,leftLen);
    memcpy(res_str+leftLen,right,rightLen);
    res_str[leftLen+rightLen] = 0;
  }
    
  my_free(left);
  my_free(right);

  return res_str;
}

char * do_sub(char *left, char *right){
  char *res_str;
  
  bc_num l = bc_new_num(10,10);
  bc_num r = bc_new_num(10,10);

  bc_num res = bc_new_num(0,0);

  bc_str2num(&l,left,10);
  bc_str2num(&r,right,10);


  bc_sub(l,r,&res,0);
     
  bc_free_num(&l);
  bc_free_num(&r);

  res_str = bc_num2str(res);

  bc_free_num(&res);
        my_free(left);
        my_free(right);

  return res_str;
}

char * do_mul(char *left, char *right){
  char *res_str;
  
  bc_num l = bc_new_num(10,10);
  bc_num r = bc_new_num(10,10);

  bc_num res = bc_new_num(10,0);

  

  bc_str2num(&l,left,10);
  bc_str2num(&r,right,10);
  
  bc_multiply(l,r,&res,100);
     
  bc_free_num(&l);
  bc_free_num(&r);

  res_str = bc_num2str(res);

  bc_free_num(&res);
        my_free(left);
        my_free(right);
  return res_str;
}

char * do_div(char *left, char *right){
  char *res_str;
  
  bc_num l = bc_new_num(10,10);
  bc_num r = bc_new_num(10,10);

  bc_num res = bc_new_num(0,0);

  bc_str2num(&l,left,10);
  bc_str2num(&r,right,10);


  bc_divide(l,r,&res,100);
     
  bc_free_num(&l);
  bc_free_num(&r);

  res_str = bc_num2str(res);

  bc_free_num(&res);
        my_free(left);
        my_free(right);
  return res_str;
}

char * do_compare(char *left, char *right,char type){
  char *res_str;
  
  bc_num l = bc_new_num(10,10);
  bc_num r = bc_new_num(10,10);

  bc_str2num(&l,left,10);
  bc_str2num(&r,right,10);

  int res = bc_compare(l,r);
  
  if((type == COMPARE_LT && res < 0)   ||
           (type == COMPARE_GT && res >0)    ||
           (type == COMPARE_LTE && res <= 0) ||
           (type == COMPARE_GTE && res >= 0) ||
           (type == COMPARE_E && res == 0)){
                res_str ="true";
  }else{
                res_str ="false";
  }
  
  bc_free_num(&l);
  bc_free_num(&r);

  my_free(left);
  my_free(right);

  return res_str;
}

char *do_negate(char *right){
  return do_mul("-1",right);
}

char *do_pow(char *left,char *right){
  char *res_str;
  
  bc_num l = bc_new_num(10,10);
  bc_num r = bc_new_num(10,10);

  bc_num res = bc_new_num(0,0);

  bc_str2num(&l,left,10);
  bc_str2num(&r,right,10);


  bc_raise(l,r,&res,100);
     
  bc_free_num(&l);
  bc_free_num(&r);

  res_str = bc_num2str(res);

  bc_free_num(&res);
        my_free(left);
        my_free(right);
  return res_str;
  
}





char * do_and(char *left, char *right){
  char *res_str;

  //done this way so that isTrue is ALWAYS called on both parameters - otherwise they would not get free'd
  char leftTrue = isTrue(left,1);
  char rightTrue = isTrue(right,1);

  if(leftTrue && rightTrue){
    res_str = "true";
  }else{
    res_str = "false";
  }

  return res_str;
}


char * do_or(char *left, char *right){
  char *res_str;

  //done this way so that isTrue is ALWAYS called on both parameters - otherwise they would not get free'd
  char leftTrue = isTrue(left,1);
  char rightTrue = isTrue(right,1);

  if(leftTrue || rightTrue){
    res_str = "true";
  }else{
    res_str = "false";
  }

  return res_str;
}


char * do_not(char *val){
  char *res_str;

  //done this way so that isTrue is ALWAYS called on both parameters - otherwise they would not get free'd
 
  if(isTrue(val,1)){
    res_str = "false";
  }else{
    res_str = "true";
  }

  return res_str;
}

char * do_get_stored(char *name){
  char *r = get_stored_value(name,stored_values);
  if(r == 0){
    return "";
  }else{
    //copy r to a new memory block so we can free it safely.
    int l = strlen(r)+1;
    char *c = (char *)malloc(l);
    memcpy(c,r,l);
    return c;
  }
}

char * do_put_stored(char *name,char *value){
  my_free(put_stored_value(name, value, stored_values));
  my_free(name);  
}

char * do_timer(char *name, char *value, char *timeout){
  if(isTrue(value,1)){
    long newTimeout = millis()+atol(timeout);
    my_free(put_stored_value(name, ltostr(newTimeout), stored_values));
    return "true";
  }else{
    if(isTrue(do_compare(do_get_stored(name), ltostr(millis()),COMPARE_LTE),1)){
      return "false";
    }else{
      return "true";
    }
  }
}


char * do_date_part(char *date,int part_code){  //part_code define prefix = PART_CODE_
  
  return "";
}


void setup_operations_ptr(struct operations_ptr *p){
  p->malloc = &malloc;
  p->free = &my_free;
  p->FP = &fix_pointer; 
  p->ltostr = &ltostr;
  p->millis = &millis;

  
  p->do_compare = &do_compare;
  p->do_add = &do_add;
  p->do_sub = &do_sub;
  p->do_mul = &do_mul;
  p->do_mod = &do_mod;
  p->do_div = &do_div;
  p->do_pow = &do_pow;
  p->do_negate = &do_negate;


  p->do_and = &do_and; 
  p->do_or = &do_or;
  p->do_not = &do_not;
  p->do_timer = &do_timer;

  p->do_get_stored = &do_get_stored;
  p->do_put_stored = &do_put_stored;

  p->do_date_part = &do_date_part;
  
  p->isTrue = &isTrue;
  p->isNumber = &isNumber;
}



