<%-- Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

--%><%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%><%
%><%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%
%><%@page contentType="text/html" pageEncoding="UTF-8"%><%
%><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formula List</title>
    </head>
    <body>
        <sql:query var="formulas" dataSource="jdbc/dynamic" sql="select formula_id,formula,binary_hash from formulas"></sql:query>
        <table>
        <c:forEach var="row" items="${formulas.rows}">
            <tr>
                <td><c:out value="${row.formula_id}"/></td>
                <td><c:out value="${row.formula}"/></td>
                <td><a target="do_delete" href="<c:url value="flush.jsp"><c:param name="formula_id" value="${row.formula_id}"/></c:url>">Delete</a></td>
                <td><c:out value="${row.binary_hash}"/></td>
            </tr>
        </c:forEach>
        </table>
    </body>
</html>
