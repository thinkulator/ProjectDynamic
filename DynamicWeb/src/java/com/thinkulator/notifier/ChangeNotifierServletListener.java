/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package com.thinkulator.notifier;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Web application lifecycle listener.
 *
 * @author hack
 */
public class ChangeNotifierServletListener implements ServletContextListener {
    
    private static ChangeNotifierService notiferService = null;

    public static ChangeNotifierService getNotiferService() {
        return notiferService;
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
        //startup the notifier service.
        synchronized(this.getClass()){
            if(notiferService == null){
                try {
                    notiferService = new ChangeNotifierService(null, 5555);
                } catch (IOException ex) {
                    throw new RuntimeException("Fatal error initialing notifer service",ex);
                }
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if(notiferService != null){
            try {
                notiferService.shutdown();
            } catch (InterruptedException |IOException ex) {
                Logger.getLogger(ChangeNotifierServletListener.class.getName()).log(Level.SEVERE,"Failre stopping Notifier Thread", ex);
            }
        }
    }
}
