/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package com.thinkulator.store;

/**
 *
 * @author hack
 */
public class CompiledFormula {
    private final String sha256sum;
    private final String hexFile;
    private int block_size;
    private int block_type;
    
    public CompiledFormula(String hexFile,String sha256sum){
        this.hexFile = hexFile;
        this.sha256sum = sha256sum;
    }

    /**
     * @return the sha256sum
     */
    public String getSha256sum() {
        return sha256sum;
    }

    /**
     * @return the block_size
     */
    public int getBlockSize() {
        return block_size;
    }

    /**
     * @return the block_type
     */
    public int getBlockType() {
        return block_type;
    }

    /**
     * @return the hexFile
     */
    public String getHexFile() {
        return hexFile;
    }

    @Override
    public int hashCode() {
        return sha256sum.hashCode(); 
    }
}
