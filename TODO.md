


Codebase TODO: 
--DONE * Convert ExpressionComplier to Library
--PARTIAL - Need to make it a config setting instead of hard coding it in another dir like I did 
--DONE * Move dynamic builder outside of ExpressionComplier directory
* Remove need for Energia - Convert to full Makefile based build/push


Work on function definitions:
* Conditional (IF, IFERROR)  --IF Done, need to add "Error" conditions.
* Text operations (Concat, detection, etc) --Concat done, defection done, rest of functions remaining.
--PARTIAL (Need to make into functions, rather than hard coded) * Time based averaging/de-noising
* Date/Time functions (YEAR, MONTH, DAY, HOUR, MINUTE, SECOND, MILLIS, DATE, TIME, DOW)


Design work:
* Design for Quantity per interval (RPM)
* Design for Total quantity (Counter)
* Design for Timing measurement

* Design for re-usable blocks "BLINK(high/low) -> (UPTIME()%(high+low))>low"

* Design multi-function outputs (outputs defined by running multiple functions per output, instead of a single one)
  - Current output handlers are done on a per formula basis.  Pluggable outputs need to be the basis, and they can have multiple formulas that need to be run.
  


Data Transmittal:
* Occasionally send snapshot of data to the server so we can test functions online before pushing them to the device.

